
# Gorilla - TimeOff.Management

This is a fork of https://github.com/timeoff-management/application. Use for a technical test.

A Web application for managing employee absences.

## Features

If you want to review the features, you can read them in: https://github.com/timeoff-management/application

## Installation

### Cloud hosting - AWS

You can use the Ansible playbook located in:

https://bitbucket.org/afreydev/gorilla-iac

You will have to set some variables editing the file "gorilla-test-iac.yml" and at least the variable key_dest, for
set the location for a ssh key to connect to ec2 instance later. You can run it like this: "ansible-playbook gorilla-test-iac.yml".

Remember set: export ANSIBLE_HOST_KEY_CHECKING=False

Remember too: Set your AWS Access Keys.

The infraestructure provisioned for that ansible playbook will be like this:

![diagrama](docs/gorilla.png)

- The developer push new code to the repo.
- Jenkins detect by a scm polling or a webhook the change.
- Jenkins deploy the new version by Docker. (See the pipeline definition).
- The infrastructure can be provisioned by an ansible playbook.

## Pipeline definition

If you want to review the CD Pipeline definition you can read it in: 

https://bitbucket.org/afreydev/gorilla-pipeline

You can set this pipeline directly in your Pipeline Plugin in the installed Jenkins by the scm option.
The Pipeline file is: Jenkinsfile.